# Using Jupyter, Tensorflow, and Virtualenv

## Make sure pip is up-to-date
> pip install --upgrade pip

## Installing Virtualenv

> pip install --user virtualenv

## Create a virtual environment
* We're using Python3, so `python` should be for some Python version >= 3
* Check your python version in the command line:
    > python --version

> python -m venv env

## Activating the new virtual environment

* Linux/MacOS
    > source env/bin/activate

* Windows
    > env\\Scripts\\activate

## Upgrade pip
> pip install --upgrade pip

## Installing Jupyter (Lab)
> pip install jupyterlab

## Adding `ipykernel`
> pip install ipykernel
> python -m ipykernel install --user --name=\<name of the kernel\>

* _Example:_
    > python -m ipykernel install --user --name=soai

## Installing Tensorflow
> pip install --upgrade tensorflow
*   _There is also a version of tensorflow which can use GPU's, but the installation involves other drivers and libraries. For simplicity, we will use the version which only supports the CPU._

## Other project dependencies

> pip install numpy matplotlib pandas sklearn
