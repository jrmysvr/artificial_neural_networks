#!/bin/bash

# Installation script - assumes you're using python 3 by default

echo "Installing virtualenv"
python3 -m pip install --user virtualenv

echo "Creating a virtual environment called 'env'"
python3 -m venv env

echo "Activating the environment"
. env/bin/activate

echo "Upgrading pip"
python -m pip install --upgrade pip

echo "Installing Jupyter (Lab) and ipykernel "
python -m pip install jupyterlab

echo "Adding kernel for this environment"
python -m pip install ipykernel
python -m ipykernel install --user --name=beginners_tf

echo "Installing Dependencies: Tensorflow, Matplotlib, Numpy..."
python -m pip install --upgrade tensorflow
python -m pip install numpy matplotlib sklearn pandas

# Alternatively: > pip install -r requirements.txt

